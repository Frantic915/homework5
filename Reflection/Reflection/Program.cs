﻿using Reflection;
using System.Diagnostics;
using Newtonsoft.Json;
{
    Stopwatch watch = new Stopwatch();
    F? f = F.Get();
    uint numberOfIterations = 100000; 


    //Console.WriteLine($"Сериализация {numberOfIterations} раз в csv: ");
    //watch.Start();
    //for(int i = 0; i < numberOfIterations; i++)
    //{
    //    Serializer.SerializeFromObjectToCSV(f);
    //}
    //watch.Stop();
    //Console.WriteLine("Время выполнения: " + watch.ElapsedMilliseconds + "мс");
    //Console.WriteLine();

    //string serilizedF = Serializer.SerializeFromObjectToCSV(f);

    //Console.WriteLine($"Десериализация {numberOfIterations} раз в csv: ");
    //watch.Start();
    //for (int i = 0; i < numberOfIterations; i++)
    //{
    //    Serializer.DeserializeFromCSVToObject(serilizedF, typeof(F));
    //}
    //watch.Stop();
    //Console.WriteLine("Время выполнения: " + watch.ElapsedMilliseconds + "мс");


    //Console.WriteLine($"Сериализация {numberOfIterations} раз в json: ");
    //watch.Start();
    //for (int i = 0; i < numberOfIterations; i++)
    //{
    //    JsonConvert.SerializeObject(f);
    //}
    //watch.Stop();
    //Console.WriteLine("Время выполнения: " + watch.ElapsedMilliseconds + "мс");
    //Console.WriteLine();

    //string serilizedF = Serializer.SerializeFromObjectToCSV(f);

    //Console.WriteLine($"Десериализация {numberOfIterations} раз из json: ");
    //watch.Start();
    //for (int i = 0; i < numberOfIterations; i++)
    //{
    //    Serializer.DeserializeFromCSVToObject(serilizedF, typeof(F));
    //}
    //watch.Stop();
    //Console.WriteLine("Время выполнения: " + watch.ElapsedMilliseconds + "мс");


    watch.Start();
    Console.WriteLine($"Вывод в консоль {numberOfIterations} раз");
    for(int i = 0; i < numberOfIterations; i++)
    {
        Console.WriteLine(f);
    }
    watch.Stop();
    Console.WriteLine("Время вывода в консоль объекта F: " + watch.ElapsedMilliseconds + "мс");
}

