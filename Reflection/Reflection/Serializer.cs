﻿using CsvHelper;
using CsvHelper.Configuration;
using System.Globalization;
using System.Reflection;
using System.Text;

namespace Reflection
{
    /// <summary> Serializer </summary>
    public static class Serializer
    {

        static CsvConfiguration _csvConfiguration = new CsvConfiguration(CultureInfo.CurrentCulture)
        {
            Delimiter = ";",
            Encoding = Encoding.UTF8,
        };

        static BindingFlags _bindingFlags = BindingFlags.DeclaredOnly
            | BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public;


        /// <summary> Serialize from object to CSV </summary>
        /// <param name="obj">any object</param>
        /// <returns>CSV</returns>
        public static string SerializeFromObjectToCSV(object obj)
        {  
            Type type = obj.GetType();
            var fields = type.GetFields(_bindingFlags);

            StringBuilder stringBuilder = new StringBuilder();
            StringWriter stream = new StringWriter(stringBuilder);
            var csvWriter = new CsvWriter(stream, _csvConfiguration);

            csvWriter.WriteField(type.Name);
            csvWriter.NextRecord();

            foreach(var field in fields)
            {
                csvWriter.WriteField(field.Name);
                csvWriter.WriteField(field.GetValue(obj));
                csvWriter.NextRecord();
            }

            csvWriter.Flush();

            return stringBuilder.ToString();
        }

        /// <summary> Deserialize from CSV to object</summary>
        /// <param name="csv">string in CSV format</param>
        /// <param name="type"></param>
        /// <returns>object</returns>
        public static object? DeserializeFromCSVToObject(string csv, Type type)
        {
            StringReader stream = new StringReader(csv);
            var csvReader = new CsvReader(stream, _csvConfiguration);
            var fields = type.GetFields(_bindingFlags);

            csvReader.Read(); // пропускаем имя типа

            var typeObject = Activator.CreateInstance(type);

            while (csvReader.Read())
            {
                string fieldName = csvReader.GetField<string>(0);
                FieldInfo? field = type.GetField(fieldName, _bindingFlags);
                if(field != null)
                {
                    try
                    {
                        object value = csvReader.GetField(field.FieldType, 1);
                        field.SetValue(typeObject, value);
                    }
                    catch(CsvHelper.TypeConversion.TypeConverterException ex)
                    {
                        throw new TypeLoadException("Не удалось создать объект заданного типа");
                    }                    
                }
                else
                {
                    throw new TypeLoadException("Не удалось создать объект заданного типа");
                }
            }

            return typeObject;
        }
    }
}
